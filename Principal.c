#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

/*********************************************************************************************************/
//declaracion de metodos internos (C)
int enmascarar_c(unsigned char *a, unsigned char *b, unsigned char *mask, int filas, int columnas); 
int validarNumeros(int largo, char *argv[], int num);
int validarRGB(int largo, char *argv[], int num);
void crearPath(char *ancho, char * alto, char *imageFile1, char pathImg1 [150]);
int validarArchivos(char *image);
//variables
char * imageFileOutputC = "salida_c.rgb";
/*********************************************************************************************************/
//declaracion de metodos internos/externos (ASM)
int enmascarar_asm(unsigned char *a, unsigned char *b, unsigned char *mask, int filas, int columnas);
extern void llenar_buffer_output(unsigned char *img1, unsigned char *img2, unsigned char *mask, unsigned char *output, int cantCaracteres);
int llenar_buffer(char *archivo, unsigned char *buffer, int cantidad);
int generar_archivo_RGB(char *archivo, unsigned char *buffer, int cantidad);
int obtenerCantCaracteresDeMas(int filas, int columnas); 
//variables
char * imageFileOutputASM = "salida_asm.rgb";
/*********************************************************************************************************/

int main(int argc, char *argv[]){
  if( argc < 6 ){
    printf("Falta(n) %d Parametro(s)\n", 6-argc);
    return -1;
  }
  else if(argc > 6){
    printf("Sobra(n) %d Parametro(s)\n", argc-6);
    return -1;
  }
  else{
    int largo = strlen(argv[1]);
    int validador = validarRGB(largo, argv, 1);
    largo = strlen(argv[2]);
    validador += validarRGB(largo, argv, 2);
    largo = strlen(argv[3]);
    validador += validarRGB(largo, argv, 3);

    largo = strlen(argv[4]);
    validador += validarNumeros(largo, argv, 4);
    largo = strlen(argv[5]);
    validador += validarNumeros(largo, argv, 5);
  
    if(validador != 0){
      return -1;
    }
    char * imageFile1 = argv[1];
    char * imageFile2 = argv[2];
    char * imageMask = argv[3];
    char * ancho = argv[4];
    char * alto = argv[5];

    char pathImg1 [150] = "images//";
    crearPath(ancho, alto, imageFile1, pathImg1);
    imageFile1 = pathImg1;

    char pathImg2 [150] = "images//";
    crearPath(ancho, alto, imageFile2, pathImg2);
    imageFile2 = pathImg2;

    char pathMask [150] = "images//";
    crearPath(ancho, alto, imageMask, pathMask);
    imageMask = pathMask;

    validador += validarArchivos(imageFile1);
    validador += validarArchivos(imageFile2);
    validador += validarArchivos(imageMask);

    if(validador != 0){
      return -1;
    }
    printf("Tenemos 5 datos: \nImagen 1: %s\nImagen 2: %s\nMascara: %s\nAncho: %s\nAlto: %s\n", imageFile1, imageFile2, imageMask, ancho, alto);

    int filas = atoi(ancho);
    int columnas = atoi(alto);
    int timeInC = enmascarar_c(imageFile1, imageFile2, imageMask, filas, columnas);
    int timeInASM = enmascarar_asm(imageFile1, imageFile2, imageMask, filas, columnas);
    
    printf("**********************************************************************\n");
    printf("* Tamaño: %dx%d * C: %fms * ASM: %fms *\n", filas, columnas, (double)timeInC/1000.0, (double)timeInASM/1000.0 );
    printf("**********************************************************************\n");
    return 0;
  }
}

/*********************************************************************************************************/
/********************************************** Funtions C ***********************************************/
/*********************************************************************************************************/

int enmascarar_c(unsigned char *a, unsigned char *b, unsigned char *mask, int filas, int columnas){
  //Temporizador
	clock_t start, stop;
	start = clock();

  size_t st=1;
  size_t stBufferBlack=1;
  size_t stBufferWhite=1;

  char *puntero = malloc(st);
  char *bufferBlack = malloc(stBufferWhite);
  char *bufferWhite = malloc(stBufferBlack);

	FILE *fileImg1 = fopen(a, "rb");
	FILE *fileImg2 = fopen(b, "rb");
  FILE *fileMask = fopen(mask,"rb");
  FILE *fileOutput = fopen(imageFileOutputC,"wb");

  while(!feof(fileMask)){
    fread(puntero,1,st,fileMask);
    fread(bufferWhite,1, stBufferWhite,fileImg1);
    fread(bufferBlack,1, stBufferBlack,fileImg2);

    if (*puntero == 0){
      fwrite(bufferBlack, 1, stBufferBlack,fileOutput);
    }
    else{
      fwrite(bufferWhite, 1, stBufferWhite,fileOutput);
    }
  }

  fclose(fileImg1);
  fclose(fileImg2);
  fclose(fileMask);
  fclose(fileOutput);
  free(puntero);
  free(bufferBlack);
  free(bufferWhite);

	stop = clock();

  return stop-start;
}

/*********************************************************************************************************/
/********************************************* Funtions ASM **********************************************/
/*********************************************************************************************************/

int enmascarar_asm(unsigned char *a, unsigned char *b, unsigned char *mask, int filas, int columnas){
  //Temporizador
	clock_t start, stop;
	start = clock();

  int cantidadMAS = obtenerCantCaracteresDeMas(filas,columnas);
  int cantidadPOSTA = filas*columnas*3;

  unsigned char * bufferImg1 = malloc(cantidadMAS);
  unsigned char * bufferImg2 = malloc(cantidadMAS);
  unsigned char * bufferMask = malloc(cantidadMAS);
  unsigned char * bufferOutput = malloc(cantidadMAS);

  llenar_buffer(a, bufferImg1, cantidadMAS);
  llenar_buffer(b, bufferImg2, cantidadMAS);
  llenar_buffer(mask, bufferMask, cantidadMAS);

  llenar_buffer_output(bufferImg1, bufferImg2, bufferMask, bufferOutput, cantidadPOSTA);
 
  generar_archivo_RGB(imageFileOutputASM, bufferOutput, cantidadMAS);

	stop = clock();

  free(bufferImg1);
  free(bufferImg2);
  free(bufferMask);
  free(bufferOutput);

  return stop-start;
}

int obtenerCantCaracteresDeMas(int filas, int columnas) {
  return filas * columnas * 3 * 4;
}

int llenar_buffer(char *archivo, unsigned char *buffer, int cantidad){
  FILE *file = fopen(archivo, "rb");
  fread(buffer, 1, cantidad, file);
  return fclose(file);
}

int generar_archivo_RGB(char *archivo, unsigned char *buffer, int cantidad){
  FILE *file = fopen(archivo, "wb+");
  fwrite(buffer, 1, cantidad, file);
  return fclose(file);
}

/***************************************** Valitaciones **************************************************/

int validarNumeros(int largo, char *argv[], int num){
    int i;
    for (i = 0; i < largo; i++){
        if(argv[num][i] < '0' || argv[num][i] > '9'){
            if(num == 4){
                printf("El ancho no es un numero\n");
            }else{
                printf("El alto no es un numero\n");
            }
            return 1;
        }
    }
    return 0;
}

int validarRGB(int largo, char *argv[], int num){
    if(largo < 5 || argv[num][largo-4] != '.' || argv[num][largo-3] != 'r' || argv[num][largo-2] != 'g' || argv[num][largo-1] != 'b'){
        if(num == 3){
            printf("La mascara no es un .rgb\n");
        }else{
            printf("La imagen %d no es un .rgb\n", num);
        }
        return 1;
    }
    return 0;
}

void crearPath(char *ancho, char *alto, char *imageFile1, char path [150]){
  strcat(path, ancho);
  strcat(path, "x");
  strcat(path, alto);
  strcat(path, ".");
  strcat(path, imageFile1);
}

int validarArchivos(char *image){
  FILE *file;
  if((file = fopen(image, "r")) == NULL){
      printf("No existe %s\n", image);
      return 1;
  }
  fclose(file);
  return 0;
}
/*********************************************************************************************************/