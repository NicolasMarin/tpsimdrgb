;must be declared for linker (ld)
global llenar_buffer_output

section .data

section	.text

llenar_buffer_output:
    push ebp
    mov ebp, esp
    mov eax, [ebp + 8]      ;img1
    mov ebx, [ebp + 12]     ;img2
    mov esi, [ebp + 16]     ;mask
    mov edx, [ebp + 20]     ;output
	mov edi, [ebp + 24]     ;cantCaracteres
	xor ecx, ecx			;contador
ciclo:
    cmp ecx, edi
    jge fin_del_ciclo
    ;los registros xmm son de 128bits.
    ;ecx = 32bits. Luego 32 * 4 = 128bits.
	movups xmm0, [eax + ecx*4]; img1
    movups xmm1, [ebx + ecx*4]; img2
    movups xmm2, [esi + ecx*4]; mask
    ;creamos el complemento de xmm2 a xmm3.
    pcmpeqd xmm3, xmm3 ;all '1' in xmm3  
    xorps xmm3, xmm2 ;maskNegada
    ;combinamos y guardamos en xmm0.
	andps xmm0, xmm2
    andps xmm1, xmm3
	orps xmm0, xmm1
    ;guardamos xmm0 en output y continusmos.
    movd [edx + ecx*4], xmm0
    inc ecx
    jmp ciclo
fin_del_ciclo:	
	pop ebp
	ret